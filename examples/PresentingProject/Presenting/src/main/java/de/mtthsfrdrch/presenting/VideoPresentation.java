package de.mtthsfrdrch.presenting;

import android.app.Presentation;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.Display;
import android.widget.VideoView;

public class VideoPresentation extends Presentation {

    // create a developer-key and enter it here, see https://developers.google.com/youtube/android/player/register
//    private final String DEVELOPER_KEY = "";
//    private final String YOUTUBE_VIDEO_ID = "BTwEZ_fcLGk";
//    private YouTubePlayerView youTubeView;

    public VideoPresentation(Context outerContext, Display display) {
        super(outerContext, display);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.presentation_youtube);

        // YouTubeAndroidPlayerAPI doesn't work with Presentation as it needs a YouTubeBaseActivity
        // so use simple VideoView with local videoFile

        VideoView videoView = (VideoView) findViewById(R.id.videoView);
        Uri video = Uri.parse("android.resource://de.mtthsfrdrch.presenting/raw/welcome");
        videoView.setVideoURI(video);
        videoView.start();

//        youTubeView = (YouTubePlayerView) findViewById(R.id.youTubeView);
//        youTubeView.initialize(DEVELOPER_KEY, this);
    }
}
