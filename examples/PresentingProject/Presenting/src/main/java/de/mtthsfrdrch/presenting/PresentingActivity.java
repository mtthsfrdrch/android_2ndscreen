package de.mtthsfrdrch.presenting;

import android.app.Activity;
import android.app.Presentation;
import android.content.Context;
import android.media.MediaRouter;
import android.os.Bundle;
import android.view.Display;

public class PresentingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presenting);

        MediaRouter mediaRouter = (MediaRouter) getSystemService(Context.MEDIA_ROUTER_SERVICE);
        mediaRouter.addCallback(MediaRouter.ROUTE_TYPE_LIVE_VIDEO, new MediaRouterCallback());

        showPresentation(mediaRouter);
    }

    private void showPresentation(MediaRouter router) {
        MediaRouter.RouteInfo route = router.getSelectedRoute(MediaRouter.ROUTE_TYPE_LIVE_VIDEO);
        if (route != null) {
            Display presentationDisplay = route.getPresentationDisplay();
            if (presentationDisplay != null) {
                Presentation presentation = new VideoPresentation(this, presentationDisplay);
                presentation.show();
            }
        }
    }

    private class MediaRouterCallback extends MediaRouter.SimpleCallback {
        @Override
        public void onRouteChanged(MediaRouter router, MediaRouter.RouteInfo info) {
            showPresentation(router);
        }
    }
}
