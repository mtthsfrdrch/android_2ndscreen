package de.mtthsfrdrch.casting;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.cast.ApplicationMetadata;
import com.google.cast.ApplicationSession;
import com.google.cast.CastContext;
import com.google.cast.CastDevice;
import com.google.cast.MediaRouteAdapter;
import com.google.cast.MediaRouteHelper;
import com.google.cast.MediaRouteStateChangeListener;
import com.google.cast.MimeData;
import com.google.cast.SessionError;

import java.io.IOException;

public class CastingActivity extends ActionBarActivity {

    private final String CAST_APP_YOUTUBE = "YouTube";
    private final String YOUTUBE_VIDEO_ID = "BTwEZ_fcLGk";

    private CastContext castContext;

    private MediaRouter mediaRouter;
    private MediaRouteSelector mediaRouteSelector;
    private MediaRouter.Callback routerCallback;
    private CastDevice castDevice;
    private MediaRouteStateChangeListener stateChangeListener;

    private ApplicationSession session;
//    private MediaProtocolMessageStream messageStream;
//
//    private CastMedia castMedia;
//    private ContentMetadata metaData;

//    private MediaRouteButton mediaRouteButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casting);

        Context applicationContext = getApplicationContext();
        castContext = new CastContext(applicationContext);

        MediaRouteHelper.registerMinimalMediaRouteProvider(castContext, new MyMediaRouteAdapter());
        mediaRouteSelector = MediaRouteHelper.buildMediaRouteSelector(MediaRouteHelper.CATEGORY_CAST);

        routerCallback = new MediaRouterCallback();
        mediaRouter = MediaRouter.getInstance(getApplicationContext());

        // using MediaRouteActionProvider instead of button
//        mediaRouteButton = (MediaRouteButton) findViewById(R.id.buttonMediaRoute);
//        mediaRouteButton.setRouteSelector(mediaRouteSelector);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mediaRouter.addCallback(mediaRouteSelector, routerCallback,
                MediaRouter.CALLBACK_FLAG_REQUEST_DISCOVERY);
    }

    @Override
    protected void onStop() {
        mediaRouter.removeCallback(routerCallback);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        MediaRouteHelper.unregisterMediaRouteProvider(castContext);
        castContext.dispose();
        super.onDestroy();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.casting, menu);

        MenuItem mediaRouteMenuItem = menu.findItem(R.id.action_cast_selector);
        MediaRouteActionProvider mediaRouteActionProvider =
                (MediaRouteActionProvider) MenuItemCompat.getActionProvider(mediaRouteMenuItem);
        mediaRouteActionProvider.setRouteSelector(mediaRouteSelector);

        return true;
    }

    public class MediaRouterCallback extends MediaRouter.Callback {
        @Override
        public void onRouteSelected(MediaRouter router, MediaRouter.RouteInfo route) {
            Log.d("Casting", "MediaRouterCallback: onRouteSelected");
            MediaRouteHelper.requestCastDeviceForRoute(route);
        }

        @Override
        public void onRouteUnselected(MediaRouter router, MediaRouter.RouteInfo route) {
            Log.d("Casting", "MediaRouter: onRouteUnselected");
            castDevice = null;
            stateChangeListener = null;
        }
    }

    public class MyMediaRouteAdapter implements MediaRouteAdapter {

        @Override
        public void onDeviceAvailable(CastDevice castDevice, String s,
                                      MediaRouteStateChangeListener mediaRouteStateChangeListener) {
            Log.d("CasMy", "CastMediaRouteAdapter: onDeviceAvailable");
            stateChangeListener = mediaRouteStateChangeListener;
            CastingActivity.this.castDevice = castDevice;
            openSession();
        }

        @Override
        public void onSetVolume(double v) {
            Log.d("Casting", "MyMediaRouteAdapter: onSetVolume");
            stateChangeListener.onVolumeChanged(v);
        }

        @Override
        public void onUpdateVolume(double v) {
            Log.d("Casting", "MyMediaRouteAdapter: onUpdateVolume");
        }
    }

    // not needed when using MediaCastActionProvider
//    protected final void setMediaRouteButtonVisible() {
//        mediaRouteButton.setVisibility(mediaRouter.isRouteAvailable(
//                mediaRouteSelector, 0) ? View.VISIBLE : View.GONE);
//    }

    /**
     * Starts a new video playback session with the current CastContext and selected device.
     */
    private void openSession() {
        session = new ApplicationSession(castContext, castDevice);

        // The below lines allow you to specify either that your application uses the default
        // implementations of the Notification and Lock Screens, or that you will be using your own.
        int flags = 0;

        // Comment out the below line if you are not writing your own Notification Screen.
        flags |= ApplicationSession.FLAG_DISABLE_NOTIFICATION;

        // Comment out the below line if you are not writing your own Lock Screen.
        flags |= ApplicationSession.FLAG_DISABLE_LOCK_SCREEN_REMOTE_CONTROL;
        session.setApplicationOptions(flags);

        session.setListener(new ApplicationSession.Listener() {

            @Override
            public void onSessionStarted(ApplicationMetadata appMetadata) {
                Log.d("Casting", "ApplicationSessionListener: onSessionStarted");
//                ApplicationChannel channel = session.getChannel();
//                if (channel == null) {
//                    return;
//                }
//                messageStream = new MediaProtocolMessageStream();
//                channel.attachMessageStream(messageStream);
//
//                if (messageStream.getPlayerState() == null) {
//                    if (castMedia != null) {
//                        loadMedia();
//                    }
//                } else {
//                    updateStatus();
//                }
            }

            @Override
            public void onSessionStartFailed(SessionError error) {
                Log.d("Casting", "ApplicationSessionListener: onSessionFailed");
            }

            @Override
            public void onSessionEnded(SessionError error) {
                Log.d("Casting", "ApplicationSessionListener: onSessionEnded");
            }
        });

        try {
            MimeData data = new MimeData("v=" + YOUTUBE_VIDEO_ID, MimeData.TYPE_TEXT);
            session.startSession(CAST_APP_YOUTUBE, data);
//            session.startSession(CAST_APP_YOUTUBE);
        } catch (IOException e) {  }
    }

    /**
     * Loads the stored media object and casts it to the currently selected device.
     */
//    protected void loadMedia() {
//        metaData.setTitle(castMedia.getTitle());
//        try {
//            MediaProtocolCommand cmd = messageStream.loadMedia(castMedia.getUrl(), metaData, true);
//            cmd.setListener(new MediaProtocolCommand.Listener() {
//
//                @Override
//                public void onCompleted(MediaProtocolCommand mPCommand) {
//                    //mPlayPauseButton.setImageResource(R.drawable.pause_button);
//                    //mPlayButtonShowsPlay = false;
//                    //onSetVolume(0.5);
//                }
//
//                @Override
//                public void onCancelled(MediaProtocolCommand mPCommand) {
//                }
//            });
//
//        } catch (IllegalStateException e) {
//            Log.e("Casting", "Problem occurred with MediaProtocolCommand during loading", e);
//        } catch (IOException e) {
//            Log.e("Casting", "Problem opening MediaProtocolCommand during loading", e);
//        }
//    }
}
