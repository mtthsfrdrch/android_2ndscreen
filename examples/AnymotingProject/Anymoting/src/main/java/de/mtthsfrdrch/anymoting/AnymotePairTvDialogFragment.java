package de.mtthsfrdrch.anymoting;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class AnymotePairTvDialogFragment extends DialogFragment implements OnClickListener,
        OnEditorActionListener {

    /** Hosting Activities need to implement this if they want callbacks. */
    public static interface AnymotePairTvDialogCallback {
        void onPinEntered(String pin);

        void onPairingCancelled();
    }

    public static final String TAG = "PAIR_TV_DIALOG";

    private AnymotePairTvDialogCallback callback;
    private EditText editTextPin;
    private Button buttonConnect;
    private Button buttonCancel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        callback = (AnymotePairTvDialogCallback) getActivity();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        callback = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dialog_anymote_input, null);

        editTextPin = (EditText) root.findViewById(R.id.editText_anymote_pin);
        editTextPin.setHint("pairing pin");
;
        buttonConnect = (Button) root.findViewById(R.id.buttonInputConnect);
        buttonCancel = (Button) root.findViewById(R.id.buttonInputCancel);

        editTextPin.requestFocus();
        editTextPin.setOnEditorActionListener(this);
        getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        buttonConnect.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);

        return root;
    }

    @Override
    public void onClick(View view) {
        if (view == buttonConnect) {
            evaluateInput();
        } else if (view == buttonCancel && callback != null) {
            callback.onPairingCancelled();
            dismiss();
        }
    }

    @Override
    public boolean onEditorAction(TextView arg0, int arg1, KeyEvent arg2) {
        evaluateInput();

        return true;
    }

    private void evaluateInput() {
        String pin = editTextPin.getText().toString();
        if (pin != null && !pin.equals("") && callback != null) {
            callback.onPinEntered(pin);
            dismiss();
        }
    }
}
