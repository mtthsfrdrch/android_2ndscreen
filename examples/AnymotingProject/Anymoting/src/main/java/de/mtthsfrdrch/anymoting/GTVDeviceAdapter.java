package de.mtthsfrdrch.anymoting;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.entertailion.java.anymote.connection.TvDevice;

import java.util.ArrayList;
import java.util.List;


public class GTVDeviceAdapter extends BaseAdapter {

    class ViewHolder {
        TextView textViewName;
        TextView textViewAddress;
    }

    private List<TvDevice> tvDevices;
    private LayoutInflater inflater;

    public GTVDeviceAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        tvDevices = new ArrayList<TvDevice>(0);
    }

    @Override
    public int getCount() {
        return tvDevices.size();
    }

    @Override
    public TvDevice getItem(int position) {
        return tvDevices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_gtvdevice, null);
            bindView(convertView);
        }

        TvDevice device = tvDevices.get(position);
        ViewHolder holder = (ViewHolder) convertView.getTag();
        holder.textViewName.setText(device.getName());
        holder.textViewAddress.setText(device.getLocation());

        return convertView;
    }

    private void bindView(View view) {
        ViewHolder holder = new ViewHolder();
        holder.textViewName = (TextView) view.findViewById(R.id.textViewName);
        holder.textViewAddress = (TextView) view.findViewById(R.id.textViewAddress);
        view.setTag(holder);
    }

    public void setTvDevices(List<TvDevice> tvDevices) {
        this.tvDevices = tvDevices;
        notifyDataSetChanged();
    }
}
