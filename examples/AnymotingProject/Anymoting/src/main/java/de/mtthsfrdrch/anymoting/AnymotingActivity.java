package de.mtthsfrdrch.anymoting;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.entertailion.android.anymote.AndroidPlatform;
import com.entertailion.java.anymote.client.AnymoteClientService;
import com.entertailion.java.anymote.client.AnymoteSender;
import com.entertailion.java.anymote.client.ClientListener;
import com.entertailion.java.anymote.client.DeviceSelectListener;
import com.entertailion.java.anymote.client.InputListener;
import com.entertailion.java.anymote.client.PinListener;
import com.entertailion.java.anymote.connection.TvDevice;
import com.entertailion.java.anymote.util.Platform;

import java.util.List;

public class AnymotingActivity extends ActionBarActivity implements AnymotePairTvDialogFragment.AnymotePairTvDialogCallback {

    private final String YOUTUBE_VIDEO_ID = "BTwEZ_fcLGk";

    private Handler handler;

    private Button playButton;
    private GTVDeviceAdapter deviceAdapter;

    private AnymoteClientService anymoteClientService;
    private AnymoteSender anymoteSender;

    private DeviceSelectListener deviceSelectListener;
    private PinListener pinListener;

    private ClientListener clientListener;
    private InputListener inputListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anymoting);

        handler = new Handler();

        playButton = (Button) findViewById(R.id.buttonPlay);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                deviceSelectListener.onDeviceSelected(deviceAdapter.getItem(i));
            }
        });
        deviceAdapter = new GTVDeviceAdapter(getLayoutInflater());
        listView.setAdapter(deviceAdapter);



        clientListener = new MyClientListener();
        inputListener = new MyInputListener();

        Platform platform = new AndroidPlatform(this);
        anymoteClientService = AnymoteClientService.getInstance(platform);
        anymoteClientService.attachClientListener(clientListener);  // client service callback
        anymoteClientService.attachInputListener(inputListener);  // user interaction callback

        anymoteClientService.selectDevice();
    }

    @Override
    public void onDestroy() {
        if (anymoteClientService != null) {
            anymoteClientService.detachClientListener(clientListener);
            anymoteClientService.detachInputListener(inputListener);
            anymoteSender = null;
        }
        super.onDestroy();
    }

    public void playVideo(View view) {
        String uri = "vnd.youtube://" + YOUTUBE_VIDEO_ID;
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        anymoteSender.sendUrl(intent.toUri(Intent.URI_INTENT_SCHEME));
    }

    private class MyClientListener implements ClientListener {

        @Override
        public void attemptToConnect(TvDevice tvDevice) {
            Log.d("Anymoting", "ClientListener.attemptToConnect");
        }

        // not called in MainThread
        @Override
        public void onConnected(final AnymoteSender sender) {
            Log.d("Anymoting", "ClientListener.onConnected");

            handler.post(new Runnable() {
                @Override
                public void run() {
                    anymoteSender = sender;
                    playButton.setEnabled(true);
                }
            });
        }

        // not called in MainThread
        @Override
        public void onDisconnected() {
            Log.d("Anymoting", "ClientListener.onDisconnected");

            handler.post(new Runnable() {
                @Override
                public void run() {
                    anymoteSender = null;
                    playButton.setEnabled(false);
                }
            });
        }

        @Override
        public void onConnectionFailed() {
            Log.d("Anymoting", "ClientListener.onConnectionFailed");
            Toast.makeText(AnymotingActivity.this, "Connection failed", Toast.LENGTH_SHORT).show();
        }
    }

    private class MyInputListener implements InputListener {

        @Override
        public void onDiscoveringDevices() {
            Log.d("Anymoting", "InputListener.onDiscoveredDevice");
        }

        // not called in MainThread
        @Override
        public void onSelectDevice(final List<TvDevice> tvDevices, DeviceSelectListener selectListener) {
            Log.d("Anymoting", "InputListener.onSelectDevice");

            deviceSelectListener = selectListener;
            handler.post(new Runnable() {

                @Override
                public void run() {
                    deviceAdapter.setTvDevices(tvDevices);
                }
            });
        }

        @Override
        public void onPinRequired(PinListener listener) {
            Log.d("Anymoting", "InputListener.onPinRequired");

            pinListener = listener;

            DialogFragment pairDialog = new AnymotePairTvDialogFragment();
            pairDialog.show(getSupportFragmentManager(), "pairDialog");
        }
    }


    // Callbacks for PairingDialog
    @Override
    public void onPinEntered(String pin) {
        pinListener.onSecretEntered(pin);
    }

    @Override
    public void onPairingCancelled() {
        pinListener.onCancel();
    }
}